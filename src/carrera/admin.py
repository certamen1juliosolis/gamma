from django.contrib import admin
from .models import *
#from .forms import *

class AdminCarrera(admin.ModelAdmin):
    list_display = ["nombre","facultad","descripcion","fecha"]
    list_filter = ["nombre","fecha"]
    search_fields = ["nombre"]

admin.site.register(Carrera,AdminCarrera)