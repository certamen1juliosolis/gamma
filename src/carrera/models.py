from django.db import models
from facultad.models import Facultad
# Create your models here.
class Carrera(models.Model):
    id_carrera = models.AutoField(primary_key=True,unique=True)
    nombre = models.CharField(max_length=100, blank=True, null=True)
    descripcion = models.CharField(max_length=500, null= True)
    estudiantes_minimos = models.IntegerField()
    facultad = models.ForeignKey(Facultad, verbose_name= "Facultad")
    fecha = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.nombre