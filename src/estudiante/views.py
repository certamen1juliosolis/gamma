from django.shortcuts import render
from .forms import RegForm
from .models import Estudiante
# Create your views here.
def inicio(request):
    form = RegForm(request.POST or None)
    if form.is_valid():
        form_data =form.cleaned_data
        nombre2 = form_data.get("nombre")
        edad2 = form_data.get("edad")
        rut2 = form_data.get("rut")
        carrera2 = form_data.get("carrera")

        objeto = Estudiante.objects.create(nombre=nombre2,edad= edad2,rut= rut2,carrera= carrera2)
    contexto = {
        "el_formulario": form,
    }
    return render(request, "inicio.html", contexto)