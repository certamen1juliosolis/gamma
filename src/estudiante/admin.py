from django.contrib import admin
from .models import *
from .forms import *

class AdminEstudiante(admin.ModelAdmin):
    list_display = ["nombre","edad","rut","carrera","fecha"]
    list_filter = ["nombre","fecha"]
    search_fields = ["nombre"]


# Register your models here.
admin.site.register(Alumno, AdminEstudiante)
#admin.site.register(Estudiante, AdminEstudiante)

#admin.site.register(Estudiante)