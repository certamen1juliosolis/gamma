from django.contrib import admin
from .models import *
#from .forms import *

class AdminAsignatura(admin.ModelAdmin):
    list_display = ["nombre","nombre_carrera","semestre_impartido","fecha"]
    list_filter = ["nombre","fecha","semestre_impartido"]
    search_fields = ["nombre"]


admin.site.register(Asignatura, AdminAsignatura)