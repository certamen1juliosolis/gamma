from django.db import models
from carrera.models import Carrera
# Create your models here.
class Asignatura(models.Model):
    id_asignatura = models.AutoField(primary_key=True,unique=True)
    nombre = models.CharField(max_length=100, blank=True, null=True)
    nombre_carrera = models.ForeignKey(Carrera, verbose_name= "Carrera")
    semestre_impartido = models.IntegerField()
    fecha = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.nombre