from django.contrib import admin
from .models import *
#from .forms import *

class AdminFacultad(admin.ModelAdmin):
    list_display = ["nombre","descripcion","ubicacion","num_min_departamentos"]
    list_filter = ["nombre","ubicacion"]
    search_fields = ["nombre"]

admin.site.register(Facultad,AdminFacultad)