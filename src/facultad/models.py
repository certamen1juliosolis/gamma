from django.db import models

# Create your models here.
class Facultad(models.Model):
    id_facultad = models.AutoField(primary_key=True,unique=True)
    nombre = models.CharField(max_length=100, blank=True, null=True, verbose_name="Nombre Facultad")
    descripcion = models.CharField(max_length=500, null= True)
    ubicacion = models.CharField(max_length=50)
    num_min_departamentos = models.IntegerField(verbose_name="Numero minimo departamentos")

    def __str__(self):
        return self.nombre